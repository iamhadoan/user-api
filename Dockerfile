# Bản image gốc làm nền 
FROM node:10.16.2-alpine

# Chỉ định folder trong Docker image sắp được tạo, từ đây tất cả hoạt động sẽ được thực hiện trong folder này.
# Chú ý: đây không phải folder trên máy tính của chúng ta.
# Folder sẽ được tạo tự động trong quá trình build.
WORKDIR /app

# Copy file package.json package-lock.json
COPY package*.json ./

# Cài đặt npm package cần thiết
RUN npm install

# Copy source code từ /user-api trên máy tính vào /app trong Docker image
COPY . .

# Chỉ định port mà container sẽ lắng nghe request
EXPOSE 3000

# Lệnh chạy application trong container
CMD node index.js
